
package binario;

import java.util.Scanner;

/**
 *
 * @author Rendon Rios Teresita
 */

public class Suma_Binaria 
{
    private int tipo;
    public  String sum_binari(String binario_1, String binario_2) 
    {
        if (binario_1 == null || binario_2 == null) return "";
        int primero = binario_1.length() - 1;
        int segundo = binario_2.length() - 1;
        StringBuilder cadena = new StringBuilder();
        int acarreo = 0;
        
        while (primero >= 0 || segundo >= 0 ) 
        {
            int suma = acarreo;
            if (primero >= 0) 
            {
                suma += binario_1.charAt(primero) - '0';
                primero--;
            }
            if (segundo >= 0) 
            {
                suma += binario_2.charAt(segundo) - '0';
                segundo--;
            }
            acarreo = suma >> 1;
            suma = suma & 1;
            cadena.append(suma == 0 ? '0' : '1');
        }
        if (acarreo > 0)
        cadena.append('1');
        cadena.reverse();
        return String.valueOf(cadena);
    }
    
    
    public void imprime(String scad)
    {
        System.out.print("");
        System.out.print("Resultado: ");
        for(int i=scad.length()-1;i>=0;i--)
          {
         System.out.print(scad.charAt(i));
         
          }
    }
    public void cadena(String suma, int nu)
    {
        
         String nueva="";
          for(int i=nu;i>0;i--)
          {
        nueva=nueva+suma.charAt(i);
          }
            imprime(nueva);    
    }
     
    public int compara(int b1, int b2)
    {
        if(b1<b2)
            return b2;
        return b1;
    }
    
   
    
    public void validacion(String numero_binario_1, String numero_binario_2)
    {
        int bin1=numero_binario_1.length(),  bin2=numero_binario_2.length();
       
        if(bin1==bin2)
        {
           tipo=bin1;
            
            int f=numero_binario_1.length();
            if(numero_binario_1.length()==8||numero_binario_1.length()==16||numero_binario_1.length()==32||numero_binario_1.length()==64)
               
                System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
                
              String c=sum_binari(numero_binario_1,numero_binario_2);
          
            cadena(c,tipo);
        }
        //tipo 8
        else if(bin1==8)
        {   
            if(bin2==16 || bin2==32 || bin2==64)
            {    System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
                tipo=compara(bin1,bin2);
                //****
                System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
                }                         
            else System.out.println("...");
        }
        
        //tipo16
        
       else if(bin1==16)
        {
            
            if(bin2==8 || bin2==32 || bin2==64)
            {    
                System.out.println("Resulta:  "+sum_binari(numero_binario_1,numero_binario_2));
                tipo=compara(bin1,bin2);
                //****
                System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
               }
            else System.out.println("...");
        }
        
        //tipo 32
      else  if(bin1==32)
        {
            if(bin2==8 || bin2==16 || bin2==64)
            {
                System.out.println("Resulta:  "+sum_binari(numero_binario_1,numero_binario_2));
                tipo=compara(bin1,bin2);
                //****
                System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
            }
            else System.out.println("...");
        }
        
        //tipo64
       else if(bin1==64)
        {
            if(bin2==8 || bin2==32 || bin2==16)
            {
                System.out.println("Resulta:  "+sum_binari(numero_binario_1,numero_binario_2));
                tipo=compara(bin1,bin2);
                //****
                System.out.println("Sumatoria:"+sum_binari(numero_binario_1,numero_binario_2));
                      }
            else System.out.println("...");
        }

       else     System.out.println("...");
   
    }
    public static void main(String args[])
    {
        Scanner leer = new Scanner(System.in);
        String primer_numero_binario, segundo_numero_binario;
        System.out.print("Binario1: ");
        primer_numero_binario = leer.next();
        System.out.print("Binario2: ");
        segundo_numero_binario = leer.next();
        System.out.println("");
        Suma_Binaria suma= new Suma_Binaria();
        
     suma.validacion(primer_numero_binario,segundo_numero_binario);
    }   
}
